VERSION = `git rev-parse --short HEAD`

default:
	podman build -t codeberg.org/kallisti5/terarocket-gemini:${VERSION} .
	podman build -t codeberg.org/kallisti5/terarocket-gemini:latest .
push:
	podman push codeberg.org/kallisti5/terarocket-gemini:${VERSION}
	podman push codeberg.org/kallisti5/terarocket-gemini:latest
test:
	podman kill terarocket-gemini-test || true
	podman rm terarocket-gemini-test || true
	podman run --name terarocket-gemini-test -p 1965:1965 codeberg.org/kallisti5/terarocket-gemini:${VERSION}
clean:
	podman kill terarocket-gemini-test || true
	podman rm terarocket-gemini-test || true
